# Brightness and gamma applet
An applet that sets brightness and gamma levels of a screen for Linux Mint Cinnamon

![](screenshots/screenshot1.png)

## Features
* Set brightness of the screen
* Set gamma of the screen
* Customizable:
  * Customize an icon shown in a panel
  * Show: all values, brightness or gamma
  * Set: minimum and maximum value thresholds

## Installation
1. Extract .zip archive to ~/.local/share/cinnamon/applets
2. Enable the applet in Cinnamon settings

## Usage
To specify a screen and outputs:

1. Right click on the applet
2. From "Screen" submenu click on a screen
3. From "Outputs" submenu click on an output